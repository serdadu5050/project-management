<?php

return [
    // Labels
    'agency'    => 'агенттік',
    'not_found' => 'Агенттік табылмады',
    'detail'    => 'Агенттіктің деректемелері',

    // Actions
    'edit'             => 'Агенттікті өңдеңіз',
    'update'           => 'Агенттікті жаңарту',
    'updated'          => 'Агенттік жаңартылды',
    'logo_change'      => 'Агенттіктің логотипін өзгерту',
    'logo_upload'      => 'Жүктеу агенттігінің логотипі',
    'logo_upload_info' => '<Strong> 200px ені жоғары </ strong> көмегімен <strong> .png </ strong> файлын жүктеңіз',

    // Attributes
    'name'    => 'Агенттіктің атауы',
    'tagline' => 'Агенттік Tagline',
    'email'   => 'Agentur E-Mail',
    'website' => 'Агенттіктің веб-сайты',
    'address' => 'Агенттіктің мекен-жайы',
    'phone'   => 'Агенттіктің телефоны',
    'logo'    => 'Агенттіктің логотипі',
];
