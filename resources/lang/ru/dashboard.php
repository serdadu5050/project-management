<?php

return [
    'project_status_stats'             => 'Жобаның статистикасы',
    'earnings_stats'                   => 'Статистикалық статистика',
    'upcoming_subscriptions_expiry'    => 'алдағы аяқталу жазылымы',
    'no_upcoming_subscriptions_expiry' => 'Алдағы 60 күнде аяқталмаған аяқталмаған жазылымдар жоқ.',
    'yearly_earnings'                  => 'жыл сайынғы еңбегі',
    'finished_projects_count'          => 'аяқталған жобалар',
    'receiveable_earnings'             => 'дебиторлық берешек',
];
