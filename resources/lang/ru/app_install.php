<?php

return [
    'header'           => 'орнату <span class="text-primary">'.config('app.name').'</span>',
    'agency_info_text' => 'Сіздің агенттігіңізді құру үшін келесі форманы толтырыңыз.',
    'admin_info_text'  => 'Әкімші тіркелгісін жасау үшін төмендегі нысанды толтырыңыз.',
    'admin_name'       => 'Әкімші аты',
    'admin_email'      => 'Әкімшінің электрондық поштасы',
    'button'           => 'қазір орнатыңыз',
];
