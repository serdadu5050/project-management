<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password'      => 'Құпия сөздер алты таңбадан кем болмауы керек және растауға сәйкес келеді.',
    'user'          => 'Бұл электрондық пошта мекенжайы бар пайдаланушыны таба алмаймыз.',
    'token'         => 'Бұл құпия сөзді қалпына келтіру белгісі жарамсыз.',
    'sent'          => 'Біз сізге парольді жібердік.',
    'reset'         => 'Құпия сөзіңіз қалпына келтірілді!',
    'back_to_login' => 'Кіруге оралу',
];
