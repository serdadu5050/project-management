<?php

return [
    'contact'   => 'байланыс',
    'phone'     => 'телефон',
    'phone_abb' => 'Tel.',
    'cellphone' => 'телефон',
    'email'     => 'E-Mail',
    'website'   => 'Webseite',
];
