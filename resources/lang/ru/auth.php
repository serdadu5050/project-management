<?php

return [
    // Profile
    'profile'         => 'Менің профилім',
    'profile_edit'    => 'Менің профайлымды өңдеңіз',
    'update_profile'  => 'Профильді жаңарту',
    'profile_updated' => 'Профиль жаңартылды.',

    // Registration
    'register'        => 'Жаңа есептік жазба жасаңыз',
    'need_account'    => 'Шотыңыз қажет пе?',
    'have_an_account' => 'Менде шот бар',

    // Login & Logout
    'login'      => 'Кіру',
    'welcome'    => 'Қош келдіңіздер :name.',
    'failed'     => 'Бұл кіру деректері біздің деректерге сәйкес келмейді.',
    'throttle'   => 'Тым көп кіру әрекеттері. Келесі секундтан кейін қайталап көріңіз.',
    'logout'     => 'шығу',
    'logged_out' => 'Сіз шығып кеттіңіз.',

    // Password
    'change_password'          => 'Құпия сөзді өзгерту',
    'password_changed'         => 'Құпия сөзіңіз өзгертілді',
    'forgot_password'          => 'Құпия сөзді ұмыттыңыз ба?',
    'reset_password'           => 'Құпия сөзді қалпына келтіру',
    'send_reset_password_link' => 'Құпия сөзді қалпына келтіру сілтемесін жіберу',
    'old_password_failed'      => 'Ескі пароль сәйкес емес!',
    'reset_password_hint'      => 'Осы пішінді толтыру арқылы құпия сөзіңізді қалпына келтіріңіз',

    // Attributes
    'email'                     => 'E-Mail',
    'password'                  => 'Passwort',
    'password_confirmation'     => 'Passwort растау',
    'old_password'              => 'Ескі пароль',
    'new_password'              => 'Жаңа құпия сөз',
    'new_password_confirmation' => 'Жаңа құпия сөзді растаңыз',

    // Authorization
    'unauthorized_access' => 'Сіз бетке кіре алмайсыз: url.',
];
