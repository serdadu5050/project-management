<?php

return [
    // Labels
    'project'              => 'жоба',
    'projects'             => 'жобалар тізімі',
    'index_title'          => ':мәртебелік жобалар тізімі',
    'work_duration'        => 'жұмыс ұзақтығы',
    'cash_in_total'        => 'Жалпы төлем алынды',
    'cash_out_total'       => 'Жалпы төлемнің кетуі',
    'all'                  => 'Барлық жобалар',
    'search'               => 'Жобаларды іздеу',
    'detail'               => 'Жоба туралы мәліметтер',
    'found'                => 'Жобалар табылды',
    'not_found'            => 'Жоба табылмады.',
    'select'               => 'Жобаны таңдаңыз',
    'empty'                => 'Жобалар тізімі бос. ',
    'back_to_show'         => 'Жобаның толық нұсқасына оралу',
    'back_to_index'        => 'Өнім тізіміне оралу',
    'receiveable_earnings' => 'қарызды төлеу',
    'payment_remaining'    => 'Қалған төлем',
    'earnings_calculation' => 'нәтижелерін есептеу',
    'additional_jobs'      => 'Жұмыспен қамтудың қосымша тізімі',
    'overall_progress'     => 'Жалпы прогресс ',

    // Payments
    'view_payments'    => 'View All Project Payments',
    'payment_summary'  => 'Zahlungssumme',
    'payment_status'   => 'Zahlungsstatus',
    'payment_statuses' => [
        'paid'        => 'bezahlt',
        'outstanding' => 'ausstehend',
    ],

    // Actions
    'create'        => 'Neues Projekt erstellen',
    'created'       => 'Neues Projekt wurde erstellt.',
    'show'          => 'Show Project Detail',
    'edit'          => 'Projekt bearbeiten',
    'update'        => 'Projekt aktualisieren',
    'updated'       => 'Projekt wurde aktualisiert.',
    'delete'        => 'Projekt löschen',
    'deleted'       => 'Projekt wurde gelöscht.',
    'undeleted'     => 'Projekt nicht gelöscht.',
    'show_jobs'     => 'Beschäftigung anzeigen',
    'update_status' => 'Update Project Status',

    'jobs_list_export_html'     => 'Export HTML',
    'jobs_export_excel'         => 'Export Excel',
    'jobs_progress_export_html' => 'Export Fortschritt',
    'sort_jobs'                 => 'sortiere Beschäftigungspriorität',

    // Attributes
    'name'           => 'Projektname',
    'description'    => 'Projektbeschreibung',
    'start_date'     => 'Startdatum',
    'end_date'       => 'Enddatum',
    'due_date'       => 'Due Date',
    'proposal_date'  => 'Datum des Angebotes',
    'project_value'  => 'Projektwert',
    'proposal_value' => 'Angebotswert',

    // Relations
    'files'         => 'Dokumentenliste',
    'jobs'          => 'Beschäftigungsliste',
    'no_jobs'       => 'Beschäftigungsliste ist leer.',
    'cost_proposal' => 'Kostenvoranschlag',
    'invoices'      => 'Rechnungsliste',
    'customer'      => 'Kunder',
    'worker'        => 'Mitarbeiter',
    'subscriptions' => 'Abonnements',
    'status'        => 'Projektstatus',
    'payments'      => 'Zahlungen',

    // Statuses
    'planned'  => 'жоспарланған',
    'progress' => 'аяқталуда',
    'done'     => 'Дайын',
    'closed'   => 'жабық',
    'canceled' => 'доғарылды',
    'on_hold'  => 'күту күйінде',
];
