<?php

return [
    'address'      => 'мекен-жай',
    'contact'      => 'байланыс',
    'street'       => 'көше',
    'rt'           => 'RT',
    'rw'           => 'RW',
    'village'      => 'ауыл',
    'district'     => 'ауданы',
    'municipality' => 'муниципалитет',
    'city'         => 'қала',
    'province'     => 'провинция',
];
