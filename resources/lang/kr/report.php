<?php

return [
    // Labels
    'report'      => '보고서',
    'sales'       => '적립 보고서',
    'view_report' => '보고서보기',
    'sales_graph' => '적립 그래프',
    'detail'      => '보고서 세부 정보',
    'profit'      => '이익',

    // Daily
    'daily'            => '일보: :date',
    'today'            => '오늘',
    'view_daily'       => '일일보기n',
    'view_daily_label' => '일일 일일보기',

    // Monthly
    'monthly'            => '월간 보고서  :year_month',
    'this_month'         => '이번 달',
    'view_monthly'       => '월별보기',
    'view_monthly_label' => '전망매월',

    // Yearly
    'yearly'            => '연간 보고서 :year',
    'this_year'         => '올해',
    'view_yearly'       => '매년보기',
    'view_yearly_label' => '보고서보기',
];
