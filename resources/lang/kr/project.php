<?php

return [
    // Labels
    'project'              => '프로젝트',
    'projects'             => '프로젝트 목록',
    'index_title'          => '지위 목록 :지위 프로젝트',
    'work_duration'        => '근무 기간 ',
    'cash_in_total'        => '총 현금',
    'cash_out_total'       => '총 현금화',
    'all'                  => '모든 프로젝트',
    'search'               => '프로젝트 검색',
    'detail'               => '프로젝트 세부 사항',
    'found'                => '발견 된 프로젝트',
    'not_found'            => '프로젝트를 찾을 수 없음',
    'select'               => '프로젝트 선택',
    'empty'                => '프로젝트 목록이 비어 있습니다',
    'back_to_show'         => '뒤로프로젝트 세부 사항',
    'back_to_index'        => '뒤로프로젝트 목록',
    'receiveable_earnings' => '채권 수익',
    'payment_remaining'    => '지불 잔액',
    'earnings_calculation' => '수입 계산',
    'additional_jobs'      => '추가 작업 목록',
    'overall_progress'     => '전체 진행',
    'new_customer'         => '신규고객님',

    // Payments
    'view_payments'    => '모든 프로젝트 지불보기',
    'payment_summary'  => '지불 요약',
    'payment_status'   => '지불 상태',
    'payment_statuses' => [
        'paid'        => '유료',
        'outstanding' => '두드러진',
    ],

    // Actions
    'create'        => '새 만들기새 프로젝트',
    'created'       => '새 프로젝트생성되었습니다',
    'show'          => '프로젝트 세부 정보 표시',
    'edit'          => '프로젝트 수정',
    'update'        => '정보갱신계획',
    'updated'       => '계획업데이트되었습니다',
    'delete'        => '삭제계획',
    'deleted'       => '계획삭제되었습니다',
    'undeleted'     => '계획삭제되지 않음',
    'show_jobs'     => '작업 표시',
    'update_status' => '프로젝트 상태 업데이트',

    'jobs_list_export_html'     => 'HTML 내보내기',
    'jobs_export_excel'         => '엑셀 내보내기',
    'jobs_progress_export_html' => '수출 진척 상황',
    'sort_jobs'                 => '수출 진척 상황',

    // Attributes
    'name'           => '프로젝트 이름',
    'description'    => '프로젝트 설명',
    'start_date'     => '시작 날짜',
    'end_date'       => '종료 날짜',
    'due_date'       => '마감일',
    'proposal_date'  => '제안 날짜',
    'project_value'  => '프로젝트 가치',
    'proposal_value' => '제안 값',
    'project_model'  =>  '프로젝트 모델',
    
    'customer_address'  =>  '고객 주소',
    'list_customer'         => '고객 명단',

    // Relations
    'files'         => '문서 목록',
    'jobs'          => '작업 목록',
    'no_jobs'       => '작업 목록이 비어 있습니다',
    'cost_proposal' => '비용 제안',
    'invoices'      => '송장 목록e',
    'collectibe_earnings' => '단체 수입',
    'customer'      => '고객',
    'new_customer'      =>'신규 고객',
    'worker'        => '노동자',
    'subscriptions' => '구독',
    'status'        => '프로젝트 상태',
    'payments'      => '지불',

    // Statuses
    'planned'  => '계획된',
    'progress' => '진행',
    'done'     => '끝난',
    'closed'   => '닫은',
    'canceled' => '취소 된',
    'on_hold'  => '보류 중',
];
