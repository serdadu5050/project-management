<?php

return [
    // Labels
    'principal'     => '주요한',
    'list'          => '교장리스트',
    'detail'        => '주요한 세부 사항',
    'search'        => '수색 교장',
    'not_found'     => '주요한찾을 수 없음',
    'empty'         => '주요한목록이 비어있다',
    'back_to_show'  => '뒤로주요한 세부 사항',
    'back_to_index' => '뒤로교장리스트',

    // Actions
    'create'         => '새 만들기',
    'created'        => '생성되었습니다',
    'show'           => '표시주요한 세부 사항',
    'edit'           => '교장 편집',
    'update'         => '정보갱신주요한',
    'updated'        => '주요한업데이트되었습니다',
    'delete'         => '삭제주요한',
    'delete_confirm' => '이걸 삭제하시겠습니까 주요한? ',
    'deleted'        => '주요한삭제되었습니다',
    'undeleted'      => '주요한삭제되지 않음',
    'undeleteable'   => '주요한삭제할 수 없는',

    // Attributes
    'name'        => '교장 이름 ',
    'website'     => '주요 웹 사이트 ',
    'description' => '주요한묘사',
];
