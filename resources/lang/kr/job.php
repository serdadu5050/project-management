<?php

return [
    // Labels
    'job'           => '일',
    'list'          => '작업 목록',
    'on_progress'   => '진행중인 직업',
    'detail'        => '작업 세부 정보',
    'search'        => '검색 작업',
    'found'         => '찾은 직업',
    'not_found'     => '일찾을 수 없음',
    'tasks'         => '작업 목록',
    'price_total'   => '총 가격',
    'tasks_count'   => '작업 수',
    'empty'         => '일목록이 비어있다',
    'back_to_index' => '뒤로작업 목록',
    'starts'        => '시작',
    'ends'          => '끝',
    'target'        => '목표',
    'actual'        => '실제',
    'duration'      => '지속',

    // Actions
    'create'                 => '새 만들기새 직업',
    'add'                    => '추가새 직업',
    'created'                => '새 직업생성되었습니다',
    'show'                   => '작업 세부 정보보기',
    'edit'                   => '작업 수정',
    'update'                 => '일정보갱신',
    'updated'                => '작업 데이터업데이트되었습니다',
    'delete'                 => '삭제일',
    'deleted'                => '일삭제되었습니다',
    'undeleted'              => '일삭제되지 않음',
    'add_from_other_project' => '다른 프로젝트의 작업 추가',
    'select_project'         => '프로젝트 선택',
    'sort_tasks'             => '작업 우선 순위 정렬',

    'created_from_other_project' => '작업이 다른 프로젝트에 추가되었습니다',

    // Attributes
    'name'              => '직업 이름',
    'description'       => '묘사',
    'progress'          => '진행',
    'worker'            => '노동자',
    'price'             => '가격',
    'type'              => '직종',
    'target_start_date' => '목표 시작일',
    'target_end_date'   => '타겟 종료일',
    'actual_start_date' => '실제 시작일',
    'actual_end_date'   => '실제 종료일',

    // Types
    'main'       => '본관',
    'additional' => '추가의',
];
