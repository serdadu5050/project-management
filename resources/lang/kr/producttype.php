<?php

return [
    // Labels
    'producttype'      => '제품 유형',
    'list'          => '제품 유형 목록 ',
    'search'        => '검색 제품 유형',
    'not_found'     => '제품 유형찾을 수 없음',
    'empty'         => '제품 유형은 비어 있습니다',
    'back_to_show'  => '뒤로제품 유형 세부 정보',
    'back_to_index' => '뒤로돌아제품 유형 목록',
    'detail'        => '제품 유형 세부 정보',
    'contact'       => '제품 유형의 연락처',

    // Actions
    'create'         => '새 만들기 신규제품 유형',
    'created'        => '신규 제품 유형 생성 성공',
    'show'           => '제품 유형 세부 정보 보기',
    'edit'           => '제품 유형 편집',
    'update'         => '정보갱신제품 유형',
    'updated'        => '정보갱신 신규 제품 유형 성공',
    'delete'         => '삭제제품 유형',
    'delete_confirm' => '이걸 삭제 하시겠습니까제품 유형? ',
    'deleted'        => '삭제제품 유형성공',
    'undeleted'      => '제품 유형삭제되지 않음',
    'undeleteable'   => '제품 유형삭제할 수 없는',

    // Attributes
    'name'           => '제품 유형 이름',
    'description'    => '제품 유형묘사',
    'pic'            => '책임자',
    'projects_count' => '프로젝트 개수',

    // Relations
    'projects'            => '프로젝트 목록',
    'payments'            => '지불 내역',
    'subscriptions'       => '구독 목록 ',
    'subscriptions_count' => '구독 횟수',
    'invoices'            => '송장목록',
];
