<?php

return [
    'project_status_stats'             => '프로젝트 상태',
    'earnings_stats'                   => '수익 보고서',
    'upcoming_subscriptions_expiry'    => '향후 구독 만료 ',
    'no_upcoming_subscriptions_expiry' => '향후 60 일 이내에 만료일은 없습니다',
    'yearly_earnings'                  => '연간 수입',
    'finished_projects_count'          => '완성 된 프로젝트 ',
    'receiveable_earnings'             => '미수 수익',
    'yearly_potential_earnings'        => '가능성연간 수입',
    'planning_stat'                    => '잠재적 인 보고서',
    'yearly_sales'                     => '연간 매출액'
];
