<?php

return [

    /*
    |---------------------------------------------------------------------------------------
    | Baris Bahasa untuk Pengingat Password
    |---------------------------------------------------------------------------------------
    |
    | Baris bahasa berikut adalah baris standar yang cocok dengan alasan yang
    | diberikan oleh pembongkar Password yang telah gagal dalam upaya pembaruan
    | Password, misalnya token tidak valid atau Password baru tidak valid.
    |
    */

    'password'      => '암호는 6 자 이상이어야하며
확인과 일치하다',
    'user'          => '이 이메일 주소를 가진 사용자를 찾을 수 없습',
    'token'         => '이 비밀번호 재설정 토큰이 잘못되었습',
    'sent'          => '비밀번호 재설정 링크를 이메일로 보냈습니다',
    'reset'         => '비밀번호가 재설정되었습니다!',
    'back_to_login' => '로그인 페이지로 돌아 가기 ',
];
