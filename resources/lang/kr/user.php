<?php

return [
    // Label
    'user'          => '사용자',
    'list'          => '사용자 목록 ',
    'user_id'       => '사용자 ID',
    
    'profile'       => '윤곽',
    'current_jobs'  => '현재 작업',
    'search'        => '사용자 검색: 형식 이름 ',
    'search'        => '사용자 이름을 입력 한 다음 Enter 키를 누릅니다.',
    'found'         => '사용자녹이다',
    'not_found'     => '사용자찾을 수 없음',
    'empty'         => '사용자 목록비었다',
    'back_to_show'  => '뒤로사용자윤곽',
    'back_to_index' => '뒤로사용자 목록',
    'calendar'      => '사용자 달력',
    'calendars'      => [
        'add_edit_event' => '클릭 더하다 / 일정 수정'
    ],


    // Actions
    'create'    => '만들기새로운 사용자',
    'created'   => '사용자생성되었습니다',
    'show'      => '사용자세부 사항',
    'edit'      => '사용자 데이터 수정',
    'update'    => '정보갱신사용자 데이터',
    'updated'   => '사용자업데이트되었습니다',
    'delete'    => '삭제사용자 데이터',
    'deleted'   => '사용자삭제되었습니다',
    'undeleted' => '사용자삭제되지 않음',

    // Attributes
    'name'          => '사용자 이름',
    'email'         => '이메일',
    'lang'          => '언어',
    'api_token'     => 'API 토큰',
    'registered_at' => '등록일',
    'position'       => '위치',

    // Roles
    'role'  => '역할',
    'roles' => [
        'admin'  => '최고 경영자',
        'worker' => '조작 가능한',
        'finance' => '재원',
        'product' => '매니저',
        
    ],

    // Relations
    'jobs'       => '직업 목록',
    'jobs_count' => '직업 수',
    'projects'   => '프로젝트 목록 ',
];
