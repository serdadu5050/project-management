<?php

return [
    // Labels
    'task'              => '태스크',
    'list'              => '작업 목록',
    'empty'             => '작업 목록이 비어 있습니다',
    'search'            => '검색 작업',
    'found'             => '태스크녹이다',
    'not_found'         => '작업을 찾을 수 없습니다',
    'back_to_index'     => '뒤로작업 목록',
    'move_to_other_job' => '다른 직업으로 이동',

    // Actions
    'create'    => '만들기새 작업',
    'created'   => '태스크생성되었습니다',
    'show'      => '태스크세부 묘사',
    'edit'      => '편집하다태스크',
    'update'    => '정보갱신태스크',
    'updated'   => '태스크업데이트되었습니다',
    'delete'    => '삭제태스크',
    'deleted'   => '태스크삭제되었습니다',
    'undeleted' => '태스크삭제되지 않음',

    // Attributes
    'name'        => '작업 이름',
    'progress'    => '진행',
    'description' => '태스크묘사',
];
