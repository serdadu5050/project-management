<?php

return [

    // Labels
    'subscription'  => '신청',
    'subscriptions' => '구독 목록',
    'list'          => '구독 목록',
    'detail'        => '구독 세부 정보',
    'search'        => '구독 정보 검색',
    'found'         => '신청녹이다',
    'not_found'     => '신청찾을 수 없음',
    'empty'         => '신청목록이 비어있다',
    'back_to_show'  => '뒤로구독 세부 정보',
    'back_to_index' => '뒤로구독 목록',

    // Actions
    'create'    => '새 만들기신규 구독',
    'created'   => '신청생성되었습니다',
    'show'      => '표시구독 세부 정보',
    'edit'      => '편집하다신청',
    'update'    => '정보갱신신청',
    'updated'   => '신청업데이트되었습니다',
    'delete'    => '삭제신청',
    'deleted'   => '신청삭제되었습니다',
    'undeleted' => '신청삭제되지 않음',

    // Attributes
    'name'             => '구독 이름',
    'price'            => '가격',
    'domain_name'      => '도메인',
    'domain_price'     => '도메인 가격',
    'epp_code'         => 'EPP 코드',
    'hosting_capacity' => '호스팅 용량',
    'hosting_price'    => '호스팅 가격',
    'start_date'       => '시작 날짜',
    'due_date'         => '마감일',
    'extension_price'  => '연장 가격',
    'project'          => '계획',
    'customer'         => '고객',
    'vendor'           => '공급 업체',
    'notes'            => '노트',
    'type'             => '구독 유형',

    // Types
    'types' => [
        'domain'      => '도메인',
        'hosting'     => '호스팅',
        'maintenance' => '유지',
    ],
];
