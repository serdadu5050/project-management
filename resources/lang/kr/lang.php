<?php

return [
    'lang'           => '언어',
    'switch_tooltip' => '스위치 언어 :lang',
    'en'             => '영어',
    'id'             => '인도네시아어',
    'kr'             => '한국어',
];
