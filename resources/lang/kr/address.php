<?php

return [
    'address'      => '주소',
    'contact'      => '접촉',
    'street'       => '거리',
    'rt'           => 'RT',
    'rw'           => 'RW',
    'village'      => '마을',
    'district'     => '구역',
    'municipality' => '시정촌',
    'city'         => '도시',
    'province'     => '주',
];
