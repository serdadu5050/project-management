<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'flash_message'        => '양식 필드를 다시 확인하십시오.',
    'accepted'             => '기입하다 :attribute 수락해야합니다',
    'active_url'           => '기입하다 :attribute 유효한 URL이 아닙니다',
    'after'                => '기입하다 :attribute 다음 날짜 여야합니다:date',
    'alpha'                => '기입하다 :attribute 문자 만 포함 할 수 있음',
    'alpha_dash'           => '기입하다 :attribute 문자, 숫자 및 대시 만 포함 할 수 있습니다',
    'alpha_num'            => '기입하다 :attribute 문자와 숫자 만 포함 할 수 있습니다',
    'array'                => '기입하다 :attribute 배열이어야합니다',
    'before'               => '기입하다 :attribute 이전 날짜 여야합니다',
    'between'              => [
        'numeric' => '기입하다 :attribute 사이에 있어야합니다:min 과 :max',
        'file'    => '기입하다 :attribute 사이에 있어야합니다:min 과 :max 킬로바이트',
        'string'  => '기입하다 :attribute사이에 있어야합니다:min 과 :max 문자들',
        'array'   => '기입하다 :attribute사이에 있어야합니다:min 과 :max 항목',
    ],
    'boolean'              => '기입하다 :attribute 필드는 true 또는 false 여야합니다',
    'confirmed'            => '기입하다 :attribute  확인이 일치하지 않습니다 ',
    'date'                 => '기입하다 :attribute 유효한 날짜가 아닙니다',
    'date_format'          => '기입하다 :attribute형식과 일치하지 않습니다:format',
    'different'            => '기입하다 :attribute 및 : other 다른해야합니다',
    'digits'               => '기입하다 :attribute 반드시 있어야한다:digits 자릿수',
    'digits_between'       => '기입하다 :attribute  사이에 있어야합니다:min 과 :max 자릿수',
    'dimensions'           => '기입하다 :attribute 잘못된 이미지 크기가 있습니다',
    'email'                => '기입하다 :attribute 유효한 이메일 주소이어야합니다',
    'exists'               => '선택된:attribute 유효하지 않다',
    'filled'               => '기입하다 :attribute 필요하다',
    'image'                => '기입하다 :attribute 이미지 여야합니다',
    'in'                   => '선택된:attribute 유효하지 않다',
    'integer'              => '선택된:attribute 정수 여야합니다',
    'ip'                   => '기입하다 :attribute 유효한 IP 주소 여야합니다',
    'json'                 => '기입하다 :attribute 유효한 JSON 문자열이어야합니다',
    'max'                  => [
        'numeric' => '선택된:attribute 다음보다 클 수 없다:max ',
        'file'    => '기입하다 :attribute 다음보다 클 수 없다:max킬로바이트',
        'string'  => '기입하다 :attribute 다음보다 클 수 없다:max문자들',
        'array'   => '선택된:attribute 다음보다 클 수 없다:max 항목',
    ],
    'mimes'                => '기입하다 :attribute 의 파일이어야합니다type: :values',
    'min'                  => [
        'numeric' => '기입하다 :attribute 최소한이어야한다:min',
        'file'    => '선택된:attribute 최소한이어야한다:min 킬로바이트',
        'string'  => '기입하다 :attribute 최소한이어야한다:min 문자',
        'array'   => '기입하다 :attribute 최소한이어야한다:min 목',
    ],
    'not_in'               => '선택된:attribute 유효하지 않다',
    'numeric'              => '기입하다 :attribute 숫자 여야합니다',
    'regex'                => '선택된:attribute  형식이 잘못되었습니다',
    'required'             => '기입하다 :attribute 필드가 필요합니다',
    'required_if'          => '기입하다 :attribute 필드는:other 이다:value ',
    'required_unless'      => '기입하다 :attribute이것은 다음을 제외하고 필수 입력란입니다:other 이다:values ',
    'required_with'        => '선택된:attribute 필드는:values is present 선물이있다',
    'required_with_all'    => '기입하다 :attribute 필드는:values  선물이있다',
    'required_without'     => '기입하다 :attribute 필드는:values 존재하지 않는다',
    'required_without_all' => '기입하다 :attribute 필드가 필요 없을 때:values 존재하다',
    'same'                 => '선택된:attribute 과:other일치해야합니다',
    'size'                 => [
        'numeric' => '기입하다 :attribute 반드시 있어야한다:size',
        'file'    => '기입하다 :attribute반드시 있어야한다:size 킬로바이트',
        'string'  => '기입하다 :attribute반드시 있어야한다:size 문자들',
        'array'   => '선택된:attribute 포함해야한다:size 항목',
    ],
    'string'               => '기입하다 :attribute 문자열이어야합니다 ',
    'timezone'             => '기입하다 :attribute 유효한 영역이어야합니다 ',
    'unique'               => '기입하다 :attribute이미 취해진 것',
    'url'                  => '기입하다 :attribute형식이 잘못되었습니다 ',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'project' => [
        'customer_name'  => [
            'required_without' => '고객 이름은 필수 항목입니다.',
        ],
        'customer_email' => [
            'required_without' => '고객 이메일이 필요합니다.',
        ],
    ],
    'agency'  => [
        'logo' => [
            'file_extension' => '와 함께 이미지를 업로드하십시오 <strong>.png</strong>체재.',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
