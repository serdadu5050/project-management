<?php

return [
    // Labels
    'payment'       => '지불',
    'payments'      => '지불 목록',
    'list'          => '지불 목록',
    'found'         => '지불 발견됨',
    'not_found'     => '지불찾을 수 없음',
    'empty'         => '지불목록이 비어있다',
    'back_to_index' => '뒤로지불 목록',
    'receipt'       => '영수증',
    'from'          => '받은 사람',
    'cash_amount'   => '현금 금액',
    'words_amount'  => '단어 금액',

    // Actions
    'create'     => '새 만들기새로운 지불',
    'create_fee' => '수수료 지불 작성',
    'created'    => '지불생성되었습니다',
    'show'       => '지불 내역보기',
    'detail'     => '지불 세부 사항',
    'edit'       => '지불 수정',
    'update'     => '결제 업데이트',
    'updated'    => '지불업데이트되었습니다',
    'delete'     => '삭제지불',
    'deleted'    => '지불삭제되었습니다',
    'undeleted'  => '지불삭제되지 않음',
    'search'     => '프로젝트 지불 검색',
    'print'      => '영수증 인쇄',

    // Attributes
    'id'          => '지불 ID',
    'description' => '묘사',
    'date'        => '지불 일',
    'in_out'      => '거래 유형',
    'in'          => '현금',
    'out'         => '현금 인출',
    'type'        => '지불 유형',
    'project'     => '계획',
    'customer'    => '에서부터',
    'amount'      => '양',
    'cash_in'     => '현금',
    'cash_out'    => '현금 인출',
    'payer'       => '지불 자',

    // Types
    'types' => [
        'project'     => '계획',
        'add_job'     => '작업 추가',
        'maintenance' => '유지',
    ],
];
