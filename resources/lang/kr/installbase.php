<?php

return [
    // Labels
    'installbases'      => '기지 설치',
    'installbase'      => '기지 설치',
    'list'          => '기지 설치 목록 ',
    'search'        => '검색 기지 설치',
    'not_found'     => '기지 설치찾을 수 없음',
    'empty'         => '기지 설치은 비어 있습니다',
    'back_to_show'  => '뒤로기지 설치 세부 정보',
    'back_to_index' => '뒤로돌아기지 설치 목록',
    'detail'        => '기지 설치 세부 정보',
    'contact'       => '기지 설치의 연락처',

    // Actions
    'create'         => '새 만들기 신규기지 설치',
    'created'        => '신규 기지 설치 생성 성공',
    'show'           => '기지 설치 세부 정보 보기',
    'edit'           => '기지 설치 편집',
    'update'         => '정보갱신기지 설치',
    'updated'        => '정보갱신 신규 기지 설치 성공',
    'delete'         => '삭제기지 설치',
    'delete_confirm' => '이걸 삭제 하시겠습니까기지 설치? ',
    'deleted'        => '삭제기지 설치성공',
    'undeleted'      => '기지 설치삭제되지 않음',
    'undeleteable'   => '기지 설치삭제할 수 없는',

    'customer'       => '고객',
    'product'       => '생성물',
    'install_date'       => '날짜',
    'pic'       => '그림',
    'contact'    => '접촉',
    'sn'    => '일련 번호',

    // Attributes
    'name'           => '기지 설치 이름',
    'description'    => '기지 설치묘사',
    'pic'            => '책임자',
    'projects_count' => '프로젝트 개수',

    // Relations
    'projects'            => '프로젝트 목록',
    'payments'            => '지불 내역',
    'subscriptions'       => '구독 목록 ',
    'subscriptions_count' => '구독 횟수',
    'invoices'            => '송장목록',
];
