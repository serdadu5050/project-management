<?php

return [
    // Labels
    'comment' => '댓글',
    'list'    => '댓글 목록',
    'empty'   => '코멘트는 빈',

    // Actions
    'create'         => '추가댓글',
    'create_text'    => '의견 쓰기',
    'created'        => '만들기새로운 의견 성공',
    'edit'           => '편집댓글',
    'update'         => '정보갱신댓글',
    'updated'        => '정보갱신 새로운 의견 성공',
    'delete'         => '삭제댓글',
    'delete_confirm' => '이걸 삭제 하시겠습니까?',
    'deleted'        => '삭제 새로운 의견 성공 ',
    'undeleted'      => '댓글삭제되지 않음',
    'undeleteable'   => '댓글삭제할 수 없는',

    // Attributes
    'body' => '댓글',
];
