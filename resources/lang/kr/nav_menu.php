<?php

return [
    'dashboard' => '계기반',
    'agency'    => '프로필 에이전시',
    'calendar'  => '달력',
    'nav'       => '지원/복원DB',

    'producttype'       => '제품 유형 ',
    'product'       => '제품',
    'install_base'       => '베이스 설치',
];
