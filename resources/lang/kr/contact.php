<?php

return [
    'contact'   => '연락처',
    'phone'     => '전화',
    'phone_abb' => '전화',
    'cellphone' => '휴대폰',
    'email'     => '이메일',
    'website'   => '웹사이트',
];
