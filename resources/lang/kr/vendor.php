<?php

return [
    // Labels
    'vendor'        => '공급 업체',
    'list'          => '공급 업체목록',
    'detail'        => '공급 업체세부 사항',
    'search'        => '검색 공급 업체',
    'not_found'     => '공급 업체를 찾을 수 없습니다.',
    'empty'         => '공급 업체 목록이 비어 있습니다.',
    'back_to_show'  => '뒤로공급 업체세부 사항',
    'back_to_index' => '뒤로공급 업체 목록',

    // Actions
    'create'         => '새 만들기새로운 공급 업체',
    'created'        => '공급 업체생성되었습니다',
    'show'           => '공급 업체 정보',
    'edit'           => '공급 업체 편집',
    'update'         => '공급 업체정보갱신',
    'updated'        => '공급 업체업데이트되었습니다',
    'delete'         => '공급 업체삭제',
    'delete_confirm' => '이걸 삭제 하시겠습니까공급 업체? ',
    'deleted'        => '공급 업체삭제되었습니다',
    'undeleted'      => '공급 업체삭제되지 않음',
    'undeleteable'   => '공급 업체삭제할 수 없는',

    // Attributes
    'name'        => '공급 업체 이름',
    'website'     => '공급 업체 웹 사이트',
    'description' => '공급 업체묘사',
];
