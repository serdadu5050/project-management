<?php

return [
    // Labels
    'bank_account'  => '은행 계좌',
    'list'          => '은행 계좌 목록',
    'empty'         => '은행 계좌 목록이 비어 있습니다',
    'back_to_index' => '돌아가기 은행 계좌 목록',

    // Actions
    'create'         => '은행 계좌를 새 만들기',
    'created'        => '새 은행 계좌가 생성되었습니다',
    'show'           => '은행 계좌 세부 정보 표시 ',
    'edit'           => '편집 은행 계좌',
    'update'         => '정보갱신은행 계좌',
    'updated'        => '은행 계좌 데이터업데이트되었습니다',
    'delete'         => '삭제은행계좌를',
    'delete_confirm' => '이걸 삭제하시겠습니까 은행 계좌?',
    'deleted'        => '은행 계좌삭제되었습니다 ',
    'undeleted'      => '은행계좌 삭제 되지 않음',
    'undeleteable'   => '은행 계좌 삭제할 수 없는',

    // Attributes
    'name'         => '은행 이름',
    'number'       => '계좌번호',
    'account_name' => '계정 이름',
    'description'  => '묘사',
];
