<?php

return [
    // Labels
    'product'      => '생성물',
    'list'          => '생성물 목록 ',
    'search'        => '검색 생성물',
    'not_found'     => '생성물찾을 수 없음',
    'empty'         => '생성물은 비어 있습니다',
    'back_to_show'  => '뒤로생성물 세부 정보',
    'back_to_index' => '뒤로돌아생성물 목록',
    'detail'        => '생성물 세부 정보',
    'contact'       => '생성물의 연락처',

    // Actions
    'create'         => '새 만들기 신규생성물',
    'created'        => '신규 생성물 생성 성공',
    'show'           => '생성물 세부 정보 보기',
    'edit'           => '생성물 편집',
    'update'         => '정보갱신생성물',
    'updated'        => '정보갱신 신규 생성물 성공',
    'delete'         => '삭제생성물',
    'delete_confirm' => '이걸 삭제 하시겠습니까생성물? ',
    'deleted'        => '삭제생성물성공',
    'undeleted'      => '생성물삭제되지 않음',
    'undeleteable'   => '생성물삭제할 수 없는',

    // Attributes
    'name'           => '생성물 이름',
    'description'    => '생성물묘사',
    'pic'            => '책임자',
    'projects_count' => '프로젝트 개수',

    // Relations
    'projects'            => '프로젝트 목록',
    'payments'            => '지불 내역',
    'subscriptions'       => '구독 목록 ',
    'subscriptions_count' => '구독 횟수',
    'invoices'            => '송장목록',
];
