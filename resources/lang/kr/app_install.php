<?php

return [
    'header'           => '설치하다 <span class="text-primary">'.config('app.name').'</span>',
    'agency_info_text' => '아래 양식을 작성하십시오대행사만들려는 경우',
    'admin_info_text'  => '아래 양식을 작성하십시오관리자 계정만들려는 경우',
    'admin_name'       => '행정인이름',
    'admin_email'      => '행정인이메일',
    'button'           => '설치하다',
];
