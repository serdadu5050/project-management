<?php

return [
    // Profile
    'profile'         => '내 프로필',
    'profile_edit'    => '편집내 프로필',
    'update_profile'  => '프로필정보갱신',
    'profile_updated' => '프로필자료는새롭게 했다',

    // Registration
    'register'        => '계정을 새 만들기',
    'need_account'    => '계정이 필요? ',
    'have_an_account' => '내 기존 계정 확인',

    // Login & Logout
    'login'      => '로그인',
    'welcome'    => '환영합니다 :name.',
    'failed'     => '이 자격 증명은 우리 기록과 일치하지 않습니다',
    'throttle'   => '로그인 시도가 너무 많습니다. 나중에 다시 시도하십시오 :seconds 초',
    'logout'     => '로그 아웃 ',
    'logged_out' => '당신은 로그아웃',

    // Password
    'change_password'          => '비밀번호 변경 ',
    'password_changed'         => '당신의 비밀번호가 변경되었습니다',
    'forgot_password'          => '비밀번호를 잊으셨나요? ',
    'reset_password'           => '비밀번호 재설정',
    'send_reset_password_link' => '비밀번호 재설정 링크 보내기',
    'old_password_failed'      => '이전 암호가 일치하지 않습니다!',
    'reset_password_hint'      => '비밀번호를 재설정하여이 양식을 작성하십시오',

    // Attributes
    'email'                     => '이메일',
    'password'                  => '비밀번호',
    'password_confirmation'     => '비밀번호 확인',
    'old_password'              => '이전 암호 ',
    'new_password'              => '새 비밀번호',
    'new_password_confirmation' => '새 암호를 확인합니다',

    // Authorization
    'unauthorized_access' => '액세스 할 수 없습니다 :url',
];
