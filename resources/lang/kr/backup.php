<?php

return [
    // Labels
    'index_title'         => '데이터베이스를 백업 관리자 ',
    'list'                => '백업 파일 목록 ',
    'file_name'           => '파일의 이름',
    'file_size'           => '파일 크기',
    'created_at'          => '에 만들어진다 ',
    'actions'             => '조치',
    'empty'               => '사용 가능한 백업 파일이 없습니다',

    // Create backup file
    'create'              => '백업 파일 생성 ',
    'created'             => '백업 파일: 파일 생성.',
    'not_created'         => '이미 이라는 백업 파일: 이름이 이미 존재 ',

    // Delete backup file
    'delete'              => '삭제하다 ',
    'delete_title'        => '삭제백업 파일이',
    'sure_to_delete_file' => '파일을 삭제 하시겠습니까? <strong>" : 파일의 이름"</strong>? ',
    'cancel_delete'       => '삭제 취소',
    'confirm_delete'      => '네, 삭제한 파일이',
    'deleted'             => '백업 파일이: 파일 이름삭제되었습니다',

    // Download backup file
    'download'            => '다운로드',

    // Restore backup
    'restore'             => '복원을',
    'restore_title'       => '복원을 데이터베이스서 파일',
    'sure_to_restore'     => '확실해, 복원을 데이터베이스서 백업 파일이"<strong>: 파일의 이름</strong>"? <br><br>꼭 확인해주세요 <strong>현재의 데이터베이스이미 백업 된</strong>.' ,
    'cancel_restore'      => '복원을 취소',
    'confirm_restore'     => '네, 복원을 데이터베이스',
    'restored'            => '백업 파일이: 파일 이름삭제되었습니다',

    // Upload backup fle
    'upload'              => '업로드하다백업 파일이',
    'uploaded'            => '백업 파일이: 파일의 이름업로드된',
];
