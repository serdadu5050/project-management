<?php

return [
    // Labels
    'agency'    => '대행사',
    'not_found' => '대행사를 찾을 수 없음',
    'detail'    => '대행사자세히',

    // Actions
    'edit'             => '편집대행사',
    'update'           => '정보갱신대행사',
    'updated'          => '대행사자료는새롭게 했다',
    'logo_change'      => '변화기관 로고',
    'logo_upload'      => '올려주기기관 로고 ',
    'logo_upload_info' => '올려주기 <strong>.png</strong> 으로 <strong>200px 너비</strong>',

    // Attributes
    'name'    => '대행사이름',
    'tagline' => '대행사태그라인',
    'email'   => '대행사이메일',
    'website' => '대행사웹사이트',
    'address' => '대행사주소',
    'phone'   => '대행사전화 번호',
    'logo'    => '대행사로고',
];
