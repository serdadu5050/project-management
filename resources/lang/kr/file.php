<?php

return [
    // Labels
    'file'           => '파일',
    'list'           => '파일 목록',
    'search'         => '검색 파일',
    'not_found'      => '파일찾을 수 없음',
    'empty'          => '파일 목록이 비어 있습니다',
    'back_to_index'  => '뒤로파일 목록',

    // Actions
    'create'         => '올려주기새로운 파일',
    'created'        => '새로운 파일생성되었습니다',
    'edit'           => '파일 편집',
    'update'         => '정보갱신',
    'updated'        => '파일업데이트되었습니다',
    'delete'         => '삭제하다파일',
    'delete_confirm' => '이걸 삭제 하시겠습니까파일?',
    'deleted'        => '파일삭제되었습니다',
    'undeleted'      => '파일삭제되지 않음',
    'undeleteable'   => '파일삭제할 수 없는',
    'select'         => '파일 선택 ',
    'upload'         => '올려주기파일',
    'download'       => '다운로드',

    // Attributes
    'title'          => '파일 이름',
    'description'    => '파일묘사',
    'size'           => '파일 크기 ',
    'updated_at'     => '업데이트 됨',
];
