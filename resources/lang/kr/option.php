<?php

return [
    // Labels
    'list'      => '설정',
    'create'    => '추가새로운 옵션',
    'created'   => '추가새로운 옵션',
    'updated'   => '선택권업데이트 됨',
    'delete'    => '삭제선택권',
    'deleted'   => '선택권삭제 된',
    'undeleted' => '선택권삭제할 수 없는',
    'key'       => '열쇠',
    'value'     => '가치',

    // Keys
    'money_sign'         => '돈 사인 ',
    'money_sign_in_word' => '돈을 기호로 단어 ',
    'money_sign_example' => '돈 기호처럼',
    'money_max_char'     => '최대 3 자',
    'money_sign_in_word_like' => '통화문장에서',
    'one_hundred'        => '백 인도네시아 루피아',
];
