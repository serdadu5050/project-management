<?php

return [
    'created'     => '생성 된 이벤트',
    'edit'        => '이벤트 수정',
    'updated'     => '행사업데이트 됨',
    'deleted'     => '행사삭제 된',
    'rescheduled' => '행사재조정 된',
];
