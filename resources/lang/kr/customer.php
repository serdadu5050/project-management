<?php

return [
    // Labels
    'customer'      => '고객',
    'list'          => '고객 목록 ',
    'search'        => '검색 고객',
    'not_found'     => '고객찾을 수 없음',
    'empty'         => '고객은 비어 있습니다',
    'back_to_show'  => '뒤로고객 세부 정보',
    'back_to_index' => '뒤로돌아고객 목록',
    'detail'        => '고객 세부 정보',
    'contact'       => '고객의 연락처',

    // Actions
    'create'         => '새 만들기 신규고객',
    'created'        => '신규 고객 생성 성공',
    'show'           => '고객 세부 정보 보기',
    'edit'           => '고객 편집',
    'update'         => '정보갱신고객',
    'updated'        => '정보갱신 신규 고객 성공',
    'delete'         => '삭제고객',
    'delete_confirm' => '이걸 삭제 하시겠습니까고객? ',
    'deleted'        => '삭제고객성공',
    'undeleted'      => '고객삭제되지 않음',
    'undeleteable'   => '고객삭제할 수 없는',

    // Attributes
    'name'           => '고객 이름',
    'description'    => '고객묘사',
    'pic'            => '책임자',
    'projects_count' => '프로젝트 개수',

    // Relations
    'projects'            => '프로젝트 목록',
    'payments'            => '지불 내역',
    'subscriptions'       => '구독 목록 ',
    'subscriptions_count' => '구독 횟수',
    'invoices'            => '송장목록',
];
