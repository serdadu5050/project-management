<?php

return [
    // Labels
    'table_no'          => '#',
    'no'                => '수',
    'label'             => '상표',
    'action'            => '조치',
    'welcome'           => '환영 합니다',
    'to'                => '까지',
    'from'              => '부터',
    'active'            => '유효한',
    'in_active'         => '에서 활성 ',
    'show_detail_title' => '세부 정보보기 :type :name',
    'status'            => '사정',
    'type'              => '유형',
    'total'             => '합계의',
    'count'             => '수를 세다',
    'remark'            => '말하다',
    'exportxls'         => '수출이 뛰어나다',

    // Action
    'add'                   => '추가',
    'submit'                => '제출',
    'update'                => '정보 갱신',
    'delete'                => '삭제하다',
    'back'                  => '뒤로',
    'cancel'                => '취소',
    'reset'                 => '리셋',
    'show'                  => '세부 정보 보기',
    'edit'                  => '편집',
    'print'                 => '인쇄',
    'search'                => '검색하다',
    'filter'                => '필터',
    'pick'                  => '선택',
    'close'                 => '닫기',
    'delete_confirm_button' => '예, 삭제하십시오! ',
    'delete_confirm'        => '이걸 삭제 하시겠습니까?',
    'change_photo'          => '변화사진',
    'done'                  => '끝난',

    // Attributes
    'name'        => '이름',
    'notes'       => '쪽지',
    'description' => '묘사',
    'code'        => '암호',
    'date'        => '날짜',
    'time'        => '시',
    'created_at'  => '에 만들어진다',
    'created_by'  => '가 한',
    'start_date'  => '시작 날짜 ',
    'end_date'    => '끝 날짜',
];
 