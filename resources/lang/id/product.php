<?php

return [
    // Labels
    'product'      => 'Product', 
    'list'          => 'Daftar Product',
    'search'        => 'Cari Product',
    'not_found'     => ' Product tidak ditemukan',
    'empty'         => 'Belum ada   Product',
    'back_to_show'  => 'Kembali ke detail   Product',
    'back_to_index' => 'Kembali ke daftar   Product',
    'detail'        => 'Detail   Product',
    'contact'       => 'Kontak   Product',

    'customer'       => 'Customer',
    'product'       => 'Produk',
    'install_date'       => 'Tanggal Instal',
    'pic'       => 'pic',
    'contact'    => 'Kontak',
    'sn'    => 'SN',
    'product_type'         => 'Tipe Product',

    // Actions
    'create'         => 'Input   Product Baru',
    'created'        => 'Input   Product baru telah berhasil.',
    'show'           => 'Detail   Product',
    'edit'           => 'Edit   Product',
    'update'         => 'Update   Product',
    'updated'        => 'Update data   Product telah berhasil.',
    'delete'         => 'Hapus   Product',
    'delete_confirm' => 'Anda yakin akan menghapus   Product ini?',
    'deleted'        => 'Hapus data   Product telah berhasil.',
    'undeleted'      => 'Data   Product gagal dihapus.',
    'undeleteable'   => 'Data   Product tidak dapat dihapus.',

    // Attributes
    'name'           => 'Nama   Product',
    'description'    => 'Deskripsi   Product',
    'pic'            => 'PIC',
    'projects_count' => 'Jml Project',

    // Relations
    'projects'            => 'List Project',
    'payments'            => 'History Pembayaran',
    'subscriptions'       => 'List Langganan',
    'subscriptions_count' => 'Jumlah Langganan',
    'invoices'            => 'List Invoice',
];
