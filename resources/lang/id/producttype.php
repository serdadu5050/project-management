<?php

return [
    // Labels
    'producttype'      => ' Tipe Product', 
    'list'          => 'Daftar  Tipe Product',
    'search'        => 'Cari  Tipe Product',
    'not_found'     => ' Tipe Product tidak ditemukan',
    'empty'         => 'Belum ada  Tipe Product',
    'back_to_show'  => 'Kembali ke detail  Tipe Product',
    'back_to_index' => 'Kembali ke daftar  Tipe Product',
    'detail'        => 'Detail  Tipe Product',
    'contact'       => 'Kontak  Tipe Product',

    'customer'       => 'Customer',
    'product'       => 'Produk',
    'install_date'       => 'Tanggal Instal',
    'pic'       => 'pic',
    'contact'    => 'Kontak',
    'sn'    => 'SN',

    // Actions
    'create'         => 'Input  Tipe Product Baru',
    'created'        => 'Input  Tipe Product baru telah berhasil.',
    'show'           => 'Detail  Tipe Product',
    'edit'           => 'Edit  Tipe Product',
    'update'         => 'Update  Tipe Product',
    'updated'        => 'Update data  Tipe Product telah berhasil.',
    'delete'         => 'Hapus  Tipe Product',
    'delete_confirm' => 'Anda yakin akan menghapus  Tipe Product ini?',
    'deleted'        => 'Hapus data  Tipe Product telah berhasil.',
    'undeleted'      => 'Data  Tipe Product gagal dihapus.',
    'undeleteable'   => 'Data  Tipe Product tidak dapat dihapus.',

    // Attributes
    'name'           => 'Nama  Tipe Product',
    'description'    => 'Deskripsi  Tipe Product',
    'pic'            => 'PIC',
    'projects_count' => 'Jml Project',

    // Relations
    'projects'            => 'List Project',
    'payments'            => 'History Pembayaran',
    'subscriptions'       => 'List Langganan',
    'subscriptions_count' => 'Jumlah Langganan',
    'invoices'            => 'List Invoice',
];
