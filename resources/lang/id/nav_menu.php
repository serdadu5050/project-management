<?php

return [
    'dashboard' => 'Dashboard',
    'agency'    => 'Profil Agensi',
    'calendar'  => 'Kalender',
    'nav'       => 'Backup/Restore DB',
    'producttype'       => 'Tipe Produk ',
    'product'       => 'Produk',
    'install_base'       => 'Instal Basis',
];
