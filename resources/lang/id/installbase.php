<?php

return [
    // Labels
    'customer'      => 'Customer',
    'list'          => 'Daftar  Instal Basis',
    'search'        => 'Cari  Instal Basis',
    'not_found'     => 'Instal Basis tidak ditemukan',
    'empty'         => 'Belum ada  Instal Basis',
    'back_to_show'  => 'Kembali ke detail  Instal Basis',
    'back_to_index' => 'Kembali ke daftar  Instal Basis',
    'detail'        => 'Detail  Instal Basis',
    'contact'       => 'Kontak  Instal Basis',

    'installbases'  => ' Instal Basis',

    'product'       => 'Produk',
    'install_date'       => 'Tanggal Instal',
    'pic'       => 'pic',
    'sn'    => 'SN',

    // Actions
    'create'         => 'Input  Instal Basis Baru',
    'created'        => 'Input  Instal Basis baru telah berhasil.',
    'show'           => 'Detail  Instal Basis',
    'edit'           => 'Edit  Instal Basis',
    'update'         => 'Update  Instal Basis',
    'updated'        => 'Update data  Instal Basis telah berhasil.',
    'delete'         => 'Hapus  Instal Basis',
    'delete_confirm' => 'Anda yakin akan menghapus  Instal Basis ini?',
    'deleted'        => 'Hapus data  Instal Basis telah berhasil.',
    'undeleted'      => 'Data  Instal Basis gagal dihapus.',
    'undeleteable'   => 'Data  Instal Basis tidak dapat dihapus.',

    // Attributes
    'name'           => 'Nama  Instal Basis',
    'description'    => 'Deskripsi  Instal Basis',
    'projects_count' => 'Jml Project',

    // Relations
    'projects'            => 'List Project',
    'payments'            => 'History Pembayaran',
    'subscriptions'       => 'List Langganan',
    'subscriptions_count' => 'Jumlah Langganan',
    'invoices'            => 'List Invoice',
];
