<?php

return [
    // Labels
    'principal'        => 'principal',
    'list'          => 'Daftar principal',
    'detail'        => 'Detail principal',
    'search'        => 'Cari principal',
    'not_found'     => 'principal tidak ditemukan',
    'empty'         => 'Belum ada principal',
    'back_to_show'  => 'Kembali ke detail principal',
    'back_to_index' => 'Kembali ke daftar principal',

    // Actions
    'create'         => 'Input principal Baru',
    'created'        => 'Input principal baru telah berhasil.',
    'show'           => 'Detail principal',
    'edit'           => 'Edit principal',
    'update'         => 'Update principal',
    'updated'        => 'Update data principal telah berhasil.',
    'delete'         => 'Hapus principal',
    'delete_confirm' => 'Anda yakin akan menghapus principal ini?',
    'deleted'        => 'Hapus data principal telah berhasil.',
    'undeleted'      => 'Data principal gagal dihapus.',
    'undeleteable'   => 'Data principal tidak dapat dihapus.',

    // Attributes
    'name'        => 'Nama principal',
    'website'     => 'Website principal',
    'description' => 'Deskripsi principal',
];
