<?php

return [
    // Labels
    'list'      => 'Pengaturan',
    'create'    => 'Buat Option Baru',
    'created'   => 'Option baru berhasil dibuat.',
    'updated'   => 'Option berhasil disimpan.',
    'delete'    => 'Hapus Option',
    'deleted'   => 'Option berhasil dihapus.',
    'undeleted' => 'Option tidak berhasil dihapus.',
    'key'       => 'Key',
    'value'     => 'Value',

    // Keys
    'money_sign'         => 'Tanda Mata Uang',
    'money_sign_in_word' => 'Tanda Mata Uang Terbilang',
    'money_sign_example' => 'Simbol Uang Seperti',
    'money_max_char'     => 'Maksimal 3 karakter',
    'money_sign_in_word_like' => 'Bilangan mata uang dapat ditulis seperti',
    'one_hundred'        => 'Seratus',
];
