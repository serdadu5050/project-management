<?php

return [
    'project_status_stats'             => 'Statistik Status Project',
    'earnings_stats'                   => 'Statistik Pendapatan',
    'upcoming_subscriptions_expiry'    => 'Langganan akan Berakhir',
    'no_upcoming_subscriptions_expiry' => 'Belum ada Langganan yang akan berakhir dalam 60 hari ke depan.',
    'yearly_earnings'                  => 'Pendapatan Tahunan',
    'finished_projects_count'          => 'Project Selesai',
    'receiveable_earnings'             => 'Piutang',
    'yearly_potential_earnings'        => 'Potensi Pendapatan Tahunan',
    'planning_stat'                    => 'Statistik Potensi Pendapatan',
    'yearly_sales'                     => 'Penjualan Tahunan'
];
