<?php

return [
    // Labels
    'installbases'  => 'Installbase',
    'installbase'      => 'Installbase',
    'list'          => 'Installbase List',
    'search'        => 'Search installbase',
    'not_found'     => 'Installbase not found.',
    'empty'         => 'Installbase is empty.',
    'back_to_show'  => 'Back to Installbase Detail',
    'back_to_index' => 'Back to Installbase List',
    'detail'        => 'Installbase Detail',
    'contact'       => 'Installbase Contact',
    
    'customer'       => 'Customer',
    'product'       => 'Product',
    'install_date'       => 'Install Date',
    'pic'       => 'Install pic',
    'sn'    => 'SN',
     
    

    // Actions
    'create'         => 'Create new Installbase',
    'created'        => 'Create new Installbase succeded.',
    'show'           => 'View Installbase Detail',
    'edit'           => 'Edit Installbase',
    'update'         => 'Update Installbase',
    'updated'        => 'Update Installbase succeded.',
    'delete'         => 'Delete Installbase',
    'delete_confirm' => 'Are you sure to delete this Installbase?',
    'deleted'        => 'Delete Installbase succeded.',
    'undeleted'      => 'Installbase not deleted.',
    'undeleteable'   => 'Installbase data cannot be deleted.',

    // Attributes 
    'name'           => 'Installbase Name',
    'description'    => 'Installbase Description',
    'pic'            => 'PIC',
    'projects_count' => 'Projects count',

    // Relations
    'projects'            => 'Project List',
    'payments'            => 'Payment History',
    'subscriptions'       => 'Subscription List',
    'subscriptions_count' => 'Subscriptions Count',
    'invoices'            => 'Invoice List',
];
