<?php

return [
    // Labels
    'principal'        => 'principal',
    'list'          => 'principal List',
    'detail'        => 'principal Detail',
    'search'        => 'Search principal',
    'not_found'     => 'principal not found.',
    'empty'         => 'principal list is empty.',
    'back_to_show'  => 'Back to principal Detail',
    'back_to_index' => 'Back to principal List',

    // Actions
    'create'         => 'Create new principal',
    'created'        => 'principal has been created.',
    'show'           => 'principal Detail',
    'edit'           => 'Edit principal',
    'update'         => 'Update principal',
    'updated'        => 'principal has been updated.',
    'delete'         => 'Delete principal',
    'delete_confirm' => 'Are you sure to delete this principal?',
    'deleted'        => 'principal has been deleted.',
    'undeleted'      => 'principal not deleted.',
    'undeleteable'   => 'principal can not be deleted.',

    // Attributes
    'name'        => 'principal Name',
    'website'     => 'principal Website',
    'description' => 'principal Description',
];
