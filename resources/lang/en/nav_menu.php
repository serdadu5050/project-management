<?php

return [
    'dashboard' => 'Dashboard',
    'agency'    => 'Agency Profile',
    'calendar'  => 'Calendar',
    'nav'       => 'Backup/Restore DB',

    'producttype'       => 'Product Type',
    'product'       => 'Product',
    'install_base'       => 'Installbase',
];
