<?php

return [
    // Labels
    'list'      => 'Settings',
    'create'    => 'Add New Option',
    'created'   => 'New Option created.',
    'updated'   => 'Option updated.',
    'delete'    => 'Delete Option',
    'deleted'   => 'Option deleted.',
    'undeleted' => 'Option cannot be deleted.',
    'key'       => 'Key',
    'value'     => 'Value',

    // Keys
    'money_sign'         => 'Money Sign',
    'money_sign_in_word' => 'Money Sign in Word',
    'money_sign_example' => 'Money sign like',
    'money_max_char'     => 'Max 3 characters',
    'money_sign_in_word_like' => 'Money sign in word like',
    'one_hundred'        => 'One Hundred',
];
