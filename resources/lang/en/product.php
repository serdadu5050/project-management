<?php

return [
    // Labels
    'product'      => 'Product',
    'list'          => 'Product List',
    'search'        => 'Search Product',
    'not_found'     => 'Product not found.',
    'empty'         => 'Product is empty.',
    'back_to_show'  => 'Back to Product Detail',
    'back_to_index' => 'Back to Product List',
    'detail'        => 'Product Detail',
    'contact'       => 'Product Contact',

    // Actions
    'create'         => 'Create new Product',
    'created'        => 'Create new Product succeded.',
    'show'           => 'View Product Detail',
    'edit'           => 'Edit Product',
    'update'         => 'Update Product',
    'updated'        => 'Update Product succeded.',
    'delete'         => 'Delete Product',
    'delete_confirm' => 'Are you sure to delete this Product?',
    'deleted'        => 'Delete Product succeded.',
    'undeleted'      => 'Product not deleted.',
    'undeleteable'   => 'Product data cannot be deleted.',

   

    // Attributes
    'name'           => 'Product Name',
    'description'    => 'Product Description',
    'pic'            => 'PIC',
    'projects_count' => 'Projects count',

    // Relations
    'projects'            => 'Project List',
    'payments'            => 'Payment History',
    'subscriptions'       => 'Subscription List',
    'subscriptions_count' => 'Subscriptions Count',
    'invoices'            => 'Invoice List',
    'product_type'         => 'Product Type'
];
