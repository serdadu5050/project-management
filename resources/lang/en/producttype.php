<?php

return [
    // Labels
    'producttype'      => 'Product Type',
    'list'          => 'Product Type List',
    'search'        => 'Search Product Type',
    'not_found'     => 'Product Type not found.',
    'empty'         => 'Product Type is empty.',
    'back_to_show'  => 'Back to Product Type Detail',
    'back_to_index' => 'Back to Product Type List',
    'detail'        => 'Product Type Detail',
    'contact'       => 'Product Type Contact',

    // Actions
    'create'         => 'Create new Product Type',
    'created'        => 'Create new Product Type succeded.',
    'show'           => 'View Product Type Detail',
    'edit'           => 'Edit Product Type',
    'update'         => 'Update Product Type',
    'updated'        => 'Update Product Type succeded.',
    'delete'         => 'Delete Product Type',
    'delete_confirm' => 'Are you sure to delete this Product Type?',
    'deleted'        => 'Delete Product Type succeded.',
    'undeleted'      => 'Product Type not deleted.',
    'undeleteable'   => 'Product Type data cannot be deleted.',

   

    // Attributes
    'name'           => 'Product Type Name',
    'description'    => 'Product Type Description',
    'pic'            => 'PIC',
    'projects_count' => 'Projects count',

    // Relations
    'projects'            => 'Project List',
    'payments'            => 'Payment History',
    'subscriptions'       => 'Subscription List',
    'subscriptions_count' => 'Subscriptions Count',
    'invoices'            => 'Invoice List',
];
