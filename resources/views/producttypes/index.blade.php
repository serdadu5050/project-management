@extends('layouts.app')

@section('title', trans('producttype.list'))

@section('content') 
<h1 class="page-header">
    <div class="pull-right">
        {{ link_to_route('producttypes.create', trans('producttype.create'), [], ['class' => 'btn btn-success']) }}
    </div>
    {{ trans('producttype.list') }}
    <small>{{ trans('app.total') }} : {{ $producttypes->total() }} {{ trans('producttype.producttype') }}</small>
</h1>

<div class="panel panel-default table-responsive">
    <div class="panel-heading">
        {{ Form::open(['method' => 'get','class' => 'form-inline']) }}
        {!! FormField::text('q', ['value' => request('q'), 'label' => trans('producttype.search'), 'class' => 'input-sm']) !!}
        {{ Form::submit(trans('producttype.search'), ['class' => 'btn btn-sm']) }}
        {{ link_to_route('producttypes.index', trans('app.reset')) }}
        {{ Form::close() }}
    </div>
    <table class="table table-condensed">
        <thead>
            <tr>
                <th class="text-center">{{ trans('app.table_no') }}</th>
                <th>{{ trans('producttype.name') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($producttypes as $key => $producttype)
            <tr>
                <td class="text-center">{{ $producttypes->firstItem() + $key }}</td>
                <td>{{ $producttype->namelink() }}</td>
            </tr>
                @endforeach
            </tbody>
        </table>
        <div class="panel-body">{{ $producttypes->appends(Request::except('page'))->render() }}</div>
    </div>
</div>
</div>
@endsection
