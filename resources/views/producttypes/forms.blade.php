@if (Request::get('action') == 'delete' && $producttype)
@php
    $dependentRecordsCount = 0;
@endphp
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">{{ trans('producttype.delete') }}</h3></div>
            <div class="panel-body">
                <label class="control-label">{{ trans('producttype.name') }}</label>
                <p>{{ $producttype->name }}</p>
                <label class="control-label">{{ trans('producttype.products_count') }}</label>
                <p>{{ $productsCount = $producttype->products()->count() }}</p>
                @php $dependentRecordsCount += $productsCount; @endphp
                {!! $errors->first('product_type_id', '<span class="form-error small">:message</span>') !!}
            </div>
            <hr style="margin:0">
            @if ($dependentRecordsCount)
                <div class="panel-body">{{ trans('producttype.undeleteable') }}</div>
            @else
                <div class="panel-body">{{ trans('app.delete_confirm') }}</div>
            @endif

            <div class="panel-footer">
                @if (!$dependentRecordsCount)
                    @can('delete', [$producttype, $dependentRecordsCount])
                        {!! FormField::delete(
                        ['route'=>['producttypes.destroy',$producttype->id]],
                        trans('app.delete_confirm_button'),
                        ['class'=>'btn btn-danger'],
                        [
                        'product_type_id' => $producttype->id,
                        'page' => request('page'),
                        'q' => request('q'),
                        ]
                        ) !!}
                    @endcan
                @endif
                {{ link_to_route('producttypes.edit', trans('app.cancel'), [$producttype->id], ['class' => 'btn btn-default']) }}
            </div>
        </div>
    </div>
</div>
@endif
