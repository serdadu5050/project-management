@extends('layouts.app')

@section('title', trans('producttype.create'))

@section('content')
<h1 class="page-header">
    <div class="pull-right">
        {{ link_to_route('producttypes.index', trans('app.cancel'), [], ['class' => 'btn btn-default']) }}
    </div>
    {{ trans('producttype.create') }}
</h1>

{!! Form::open(['route' => 'producttypes.store']) !!}
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        {!! FormField::text('name', ['required' => true]) !!}
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                {!! Form::submit(trans('producttype.create'), ['class' => 'btn btn-success']) !!}
                {{ link_to_route('producttypes.index', trans('app.cancel'), [], ['class' => 'btn btn-default']) }}
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection
