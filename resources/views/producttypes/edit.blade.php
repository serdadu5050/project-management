@extends('layouts.app')

@section('title', trans('producttype.edit').' '.$producttype->name)

@section('content')
<h1 class="page-header">
    <div class="pull-right">
        {{ link_to_route('producttypes.index', trans('app.cancel'), [], ['class' => 'btn btn-default']) }}
    </div>
    {{ $producttype->name }} <small>{{ trans('producttype.edit') }}</small>
</h1>

@if (Request::has('action'))
    @include('producttypes.forms')
@else
{!! Form::model($producttype, ['route' => ['producttypes.update', $producttype->id],'method' => 'patch']) !!}
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        {!! FormField::text('name', ['required' => true]) !!}
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                {!! Form::submit(trans('producttype.update'), ['class' => 'btn btn-success']) !!}
                {{ link_to_route('producttypes.index', trans('app.cancel'), [], ['class' => 'btn btn-default']) }}
                {!! link_to_route('producttypes.edit', trans('app.delete'), [$producttype->id, 'action' => 'delete'], [
                    'id' => 'del-producttype-'.$producttype->id,
                    'class' => 'btn btn-link pull-right'
                ] ) !!}
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endif
@endsection
