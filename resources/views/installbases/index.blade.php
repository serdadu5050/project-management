@extends('layouts.app')

@section('title', trans('installbase.list'))

@section('content')
<h1 class="page-header">
    <div class="pull-right">
        {{ link_to_route('installbases.create', trans('installbase.create'), [], ['class' => 'btn btn-success']) }}
    </div>
    {{ trans('installbase.list') }}
    <small>{{ trans('app.total') }} : {{ $installbases->total() }} {{ trans('installbase.installbase') }}</small>
</h1>

<div class="panel panel-default table-responsive">
    <div class="panel-heading">
        {{ Form::open(['method' => 'get', 'class' => 'form-inline']) }}
        {{ Form::select('customer_id', ['' => '-- '.trans('installbase.customer').' --'] + $customers, request('customer_id'), ['class' => 'form-control', 'id' => 'customer_id']) }}
        {{ Form::submit(__('app.filter'), ['class' => 'btn btn-info btn-sm']) }}
        {{ link_to_route('installbases.index', __('app.reset'), [], ['class' => 'btn btn-default btn-sm']) }}
        {{ Form::close() }}
    </div>
    <table class="table table-condensed">
        <thead>
            <tr>
                <th class="text-center">{{ trans('app.table_no') }}</th>
                <th>{{ trans('installbase.customer') }}</th>
                <th>{{ trans('installbase.product') }}</th>
                <th>{{ trans('installbase.install_date') }}</th>
                <th>{{ trans('installbase.pic') }}</th>
                <th>{{ trans('installbase.contact') }}</th>
                <th>{{ trans('installbase.sn') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($installbases as $key => $installbase)
            <tr>
                <td class="text-center">{{ $installbases->firstItem() + $key }}</td>
                <td>{{ $installbase->nameLink() }}</td>
                <td>{{ $installbase->product->name }}</td>
                <td>{{ $installbase->install_date }}</td> 
                <td>{{ $installbase->pic }}</td>
                <td>{{ $installbase->contact }}</td>
                <td>{{ $installbase->sn }}</td>
            </tr>
                @endforeach
            </tbody>
        </table>
        <div class="panel-body">{{ $installbases->appends(Request::except('page'))->render() }}</div>
    </div>
</div>
</div>
@endsection
