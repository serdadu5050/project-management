@if (Request::get('action') == 'delete' && $installbase)
@php
    $dependentRecordsCount = 0;
@endphp
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">{{ trans('installbase.delete') }}</h3></div>
            <div class="panel-body">
                <label class="control-label">{{ trans('installbase.customer') }}</label>
                <p>{{ $installbase->customer->name }}</p>
                <label class="control-label">{{ trans('installbase.produck') }}</label>
                <p>{{ $installbase->product->name }}</p>
                <label class="control-label">{{ trans('installbase.install_date') }}</label>
                <p>{{ $installbase->install_date }}</p>
                <label class="control-label">{{ trans('installbase.pic') }}</label>
                <p>{{ $installbase->pic }}</p>
                <label class="control-label">{{ trans('installbase.contact') }}</label>
                <p>{{ $installbase->contact }}</p>
                <label class="control-label">{{ trans('installbase.sn') }}</label>
                <p>{{ $installbase->sn }}</p>
            </div>
            <hr style="margin:0">
            @if ($dependentRecordsCount)
                <div class="panel-body">{{ trans('installbase.undeleteable') }}</div>
            @else
                <div class="panel-body">{{ trans('app.delete_confirm') }}</div>
            @endif
            <div class="panel-footer">
                @can('delete', [$installbase, $dependentRecordsCount])
                    {!! FormField::delete(
                        ['route'=>['installbases.destroy',$installbase->id]],
                        trans('app.delete_confirm_button'),
                        ['class'=>'btn btn-danger'],
                        [
                            'installbase_id' => $installbase->id,
                            'page' => request('page'),
                            'q' => request('q'),
                        ]
                    ) !!}
                @endcan
                {{ link_to_route('installbases.edit', trans('app.cancel'), [$installbase->id], ['class' => 'btn btn-default']) }}
            </div>
        </div>
    </div>
</div>
@endif
