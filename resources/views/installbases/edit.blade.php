@extends('layouts.app')

@section('title', trans('installbase.edit').' '.$installbase->customer->name)

@section('content')
<h1 class="page-header">
    <div class="pull-right">
        {{ link_to_route('installbases.index', trans('app.cancel'), [], ['class' => 'btn btn-default']) }}
    </div>
    {{ $installbase->customer->name }} <small>{{ trans('installbase.edit') }}</small>
</h1>

@if (Request::has('action'))
    @include('installbases.forms')
@else
{!! Form::model($installbase, ['route' => ['installbases.update', $installbase->id],'method' => 'patch']) !!}
<div class="row">
    <div class="col-md-6 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                    {!! FormField::select('customer_id', $customers, ['placeholder' => trans('installbase.customer'),'label' => trans('installbase.customer')]) !!}
                        {!! FormField::select('product_id', $products, ['placeholder' => trans('installbase.product'),'label' => trans('installbase.product')]) !!}
                        {!! FormField::text('install_date', ['label' => trans('installbase.install_date')]) !!}
                        {!! FormField::text('pic', []) !!}
                        {!! FormField::text('contact', []) !!}
                        {!! FormField::text('sn', []) !!}
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                {!! Form::submit(trans('installbase.update'), ['class' => 'btn btn-success']) !!}
                {{ link_to_route('installbases.index', trans('app.cancel'), [], ['class' => 'btn btn-default']) }}
                {!! link_to_route('installbases.edit', trans('app.delete'), [$installbase->id, 'action' => 'delete'], [
                    'id' => 'del-installbase-'.$installbase->id,
                    'class' => 'btn btn-link pull-right'
                ] ) !!}
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endif
@endsection

@section('ext_css')
    {!! Html::style(url('assets/css/plugins/jquery.datetimepicker.css')) !!}
@endsection

@section('ext_js')
    {!! Html::script(url('assets/js/plugins/jquery.datetimepicker.js')) !!}
@endsection

@section('script')
<script>
(function() {
    $('#install_date').datetimepicker({
        timepicker:false,
        format:'Y-m-d',
        closeOnDateSelect: true,
        scrollInput: false
    });
})();
</script>
@endsection