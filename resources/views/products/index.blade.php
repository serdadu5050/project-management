@extends('layouts.app')

@section('title', trans('product.list'))

@section('content')
<h1 class="page-header">
    <div class="pull-right">
        {{ link_to_route('products.create', trans('product.create'), [], ['class' => 'btn btn-success']) }}
    </div>
    {{ trans('product.list') }}
    <small>{{ trans('app.total') }} : {{ $products->total() }} {{ trans('product.product') }}</small>
</h1>

<div class="panel panel-default table-responsive">
    <div class="panel-heading">
        {{ Form::open(['method' => 'get','class' => 'form-inline']) }}
        {!! FormField::text('q', ['value' => request('q'), 'label' => trans('product.search'), 'class' => 'input-sm']) !!}
        {{ Form::submit(trans('product.search'), ['class' => 'btn btn-sm']) }}
        {{ link_to_route('products.index', trans('app.reset')) }}
        {{ Form::close() }}
    </div>
    <table class="table table-condensed">
        <thead>
            <tr>
                <th class="text-center">{{ trans('app.table_no') }}</th>
                <th>{{ trans('product.name') }}</th>
                <th>{{ trans('product.product_type') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $key => $product)
            <tr>
                <td class="text-center">{{ $products->firstItem() + $key }}</td>
                <td>{{ $product->namelink() }}</td>
                <td>{{ $product->producttype->name }}</td>
            </tr>
                @endforeach
            </tbody>
        </table>
        <div class="panel-body">{{ $products->appends(Request::except('page'))->render() }}</div>
    </div>
</div>
</div>
@endsection
