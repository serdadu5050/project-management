@extends('layouts.app')

@section('title', trans('product.edit').' '.$product->name)

@section('content')
<h1 class="page-header">
    <div class="pull-right">
        {{ link_to_route('products.index', trans('app.cancel'), [], ['class' => 'btn btn-default']) }}
    </div>
    {{ $product->name }} <small>{{ trans('product.edit') }}</small>
</h1>

@if (Request::has('action'))
    @include('products.forms')
@else
{!! Form::model($product, ['route' => ['products.update', $product->id],'method' => 'patch']) !!}
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        {!! FormField::text('name', ['required' => true]) !!}
                        {!! FormField::select('product_type_id', $producttypes, ['label' => __('product.product_type')]) !!}
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                {!! Form::submit(trans('product.update'), ['class' => 'btn btn-success']) !!}
                {{ link_to_route('products.index', trans('app.cancel'), [], ['class' => 'btn btn-default']) }}
                {!! link_to_route('products.edit', trans('app.delete'), [$product->id, 'action' => 'delete'], [
                    'id' => 'del-product-'.$product->id,
                    'class' => 'btn btn-link pull-right'
                ] ) !!}
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endif
@endsection
