@if (Request::get('action') == 'delete' && $product)
@php
    $dependentRecordsCount = 0;
@endphp
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">{{ trans('product.delete') }}</h3></div>
            <div class="panel-body">
                <label class="control-label">{{ trans('product.name') }}</label>
                <p>{{ $product->name }}</p>
                <label class="control-label">{{ trans('producttype.name') }}</label>
                <p>{{ $product->producttype->name }}</p>
                <label class="control-label">{{ trans('product.products_count') }}</label>
                <p>{{ $productsCount = $product->installbase()->count() }}</p>
                @php $dependentRecordsCount += $productsCount; @endphp
                {!! $errors->first('product_id', '<span class="form-error small">:message</span>') !!}
            </div>
            <hr style="margin:0">
            @if ($dependentRecordsCount)
                <div class="panel-body">{{ trans('product.undeleteable') }}</div>
            @else
                <div class="panel-body">{{ trans('app.delete_confirm') }}</div>
            @endif

            <div class="panel-footer">
                @if (!$dependentRecordsCount)
                    @can('delete', [$product, $dependentRecordsCount])
                        {!! FormField::delete(
                            ['route'=>['products.destroy',$product->id]],
                            trans('app.delete_confirm_button'),
                            ['class'=>'btn btn-danger'],
                            [
                                'product_id' => $product->id,
                                'page' => request('page'),
                                'q' => request('q'),
                            ]
                        ) !!}
                    @endcan
                @endif
                {{ link_to_route('products.edit', trans('app.cancel'), [$product->id], ['class' => 'btn btn-default']) }}
            </div>
        </div>
    </div>
</div>
@endif
