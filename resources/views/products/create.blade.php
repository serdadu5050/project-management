@extends('layouts.app')

@section('title', trans('product.create'))

@section('content')
<h1 class="page-header">
    <div class="pull-right">
        {{ link_to_route('products.index', trans('app.cancel'), [], ['class' => 'btn btn-default']) }}
    </div>
    {{ trans('product.create') }}
</h1>

{!! Form::open(['route' => 'products.store']) !!}
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        {!! FormField::text('name', ['required' => true]) !!}
                        {!! FormField::select('product_type_id', $producttypes, ['placeholder' => trans('product.product_type'),'label' => trans('product.product_type')]) !!}
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                {!! Form::submit(trans('product.create'), ['class' => 'btn btn-success']) !!}
                {{ link_to_route('products.index', trans('app.cancel'), [], ['class' => 'btn btn-default']) }}
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection