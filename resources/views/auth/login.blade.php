@extends('layouts.login')

@section('title', __('auth.login'))

@section('content')
        <div class="form-group">
            {{ Form::open(['route' => 'auth.login']) }}
            
            {!! FormField::email('email', ['class' => 'form-control','label' => false, 'placeholder'=> __('auth.email')]) !!}
            
            {!! FormField::password('password', ['class' => 'form-control', 'label' => false, 'placeholder'=> __('auth.password')]) !!}
            
            <div class="form-group">
                <div class="peers ai-c jc-sb fxw-nw">
                    <div class="peer">
                        {{ Form::submit(__('auth.login'), ['class' => 'btn btn-primary btn-block']) }}
                    </div>
                    <div class="peer">
                        {{ link_to_route('auth.reset-request', __('auth.forgot_password'), [], ['class' => 'btn btn-link']) }}
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>

@endsection
 