<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="x-csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title', config('app.name'))</title>
    <style>#loader{transition:all .3s ease-in-out;opacity:1;visibility:visible;position:fixed;height:100vh;width:100%;background:#fff;z-index:90000}#loader.fadeOut{opacity:0;visibility:hidden}.spinner{width:40px;height:40px;position:absolute;top:calc(50% - 20px);left:calc(50% - 20px);background-color:#333;border-radius:100%;-webkit-animation:sk-scaleout 1s infinite ease-in-out;animation:sk-scaleout 1s infinite ease-in-out}@-webkit-keyframes sk-scaleout{0%{-webkit-transform:scale(0)}100%{-webkit-transform:scale(1);opacity:0}}@keyframes sk-scaleout{0%{-webkit-transform:scale(0);transform:scale(0)}100%{-webkit-transform:scale(1);transform:scale(1);opacity:0}}</style>
    {!! Html::style('assets/css/stylelogin.css') !!}
    @yield('ext_css')
  </head>
    <body class="app">
      <div id="loader">
        <div class="spinner">

        </div>
      </div>
      
      <script>window.addEventListener('load', () => {
          const loader = document.getElementById('loader');
          setTimeout(() => {
            loader.classList.add('fadeOut');
          }, 300);
        });
      </script>
      
      <div class="peers ai-s fxw-nw h-100vh">
        <div class="d-n@sm- peer peer-greed h-100 pos-r bgr-n bgpX-c bgpY-c bgsz-cv" style="background-image:url(assets/imgs/hnt_bg.jpg)">
          <div class="pos-a centerXY">
            <div class="bgc-white bdrs-50p pos-r" style="width:160px;height:160px">
            {!! Html::image('assets/imgs/logomini.png','',array('class' => 'pos-a centerXY')) !!}
            </div>
          </div>
        </div>
        <div class="col-12 col-md-4 peer pX-40 pY-80 h-100 bgc-white scrollable pos-r" style="min-width:320px">
          <h4 class="fw-300 c-grey-900 mB-40">@yield('title', config('app.name'))</h4>
            @yield('content')
            @include('layouts.partials.noty')
        </div>
      </div>
        {!! Html::script(url('assets/js/vendor.js')) !!}
        {!! Html::script(url('assets/js/bundle.js')) !!}
        
        @yield('ext_js')
    </body>
</html>