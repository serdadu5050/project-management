<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    protected $guard = 'user';
    protected $table = 'users';

    protected $fillable = [    	
        'name',
        'position',
        'email',
        'password',
        'remember_token',
        'api_token',
        'lang',
        'created_at',
        'updated_at'
    ];
}
