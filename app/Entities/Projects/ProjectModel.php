<?php

namespace App\Entities\Projects;

use Illuminate\Database\Eloquent\Model;

class ProjectModel extends Model
{
    protected $fillable = ['name','in_kr','in_id','in_en'];
    protected $table = 'project_model';

    /**
     * Project Model has many Project relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
