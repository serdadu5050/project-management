<?php

namespace App\Entities\Products;

use Illuminate\Database\Eloquent\Model;
// use App\Entities\Partners\Customer;

class InstallBase extends Model
{
    protected $fillable = ['customer_id','product_id','install_date','pic','contact','sn'];
    protected $table = 'install_bases';
 
    /**
     * Project Model has many Project relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Entities\Partners\Customer','customer_id','id');
    }

    public function nameLink()
    {
        return link_to_route('installbases.edit', $this->customer->name, [$this->id], [
            'title' => trans(
                'app.show_detail_title',
                ['name' => $this->customer->name, 'type' => trans('installbase.installbase')]
            ),
        ]);
    }

}
