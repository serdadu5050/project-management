<?php

namespace App\Entities\Products;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name','product_type_id'];

    /**
     * Project Model has many Project relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function producttype()
    {
        return $this->belongsTo(ProductType::class,'product_type_id','id');
    }

    public function installbase()
    {
        return $this->hasMany(InstallBase::class,'product_id','id');
    }

    public function nameLink()
    {
        return link_to_route('products.edit', $this->name, [$this->id], [
            'title' => trans(
                'app.show_detail_title',
                ['name' => $this->name, 'type' => trans('product.product')]
            ),
        ]);
    }
}
