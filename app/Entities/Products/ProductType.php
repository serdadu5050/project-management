<?php

namespace App\Entities\Products;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $fillable = ['name'];
    protected $table = 'product_type';

    /**
     * Project Model has many Project relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function nameLink()
    {
        return link_to_route('producttypes.edit', $this->name, [$this->id], [
            'title' => trans(
                'app.show_detail_title',
                ['name' => $this->name, 'type' => trans('producttype.producttype')]
            ),
        ]);
    }
}
