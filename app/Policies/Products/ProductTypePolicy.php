<?php

namespace App\Policies\Products;

use App\Entities\Users\User;
use App\Entities\Products\ProductType;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user.
     *
     * @param \App\Entities\Users\User $user
     * @param \App\Entities\Users\User $user
     *
     * @return mixed
     */
    public function view(User $user, ProductType $producttype)
    {
        return true;
    }

    /**
     * Determine whether the user can create users.
     *
     * @param \App\Entities\Users\User $user
     * @param \App\Entities\Users\User $user
     *
     * @return mixed
     */
    public function create(User $user, ProductType $producttype)
    {
        return true;
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param \App\Entities\Users\User $user
     * @param \App\Entities\Users\User $user
     *
     * @return mixed
     */
    public function update(User $user, ProductType $producttype)
    {
        return true;
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param \App\Entities\Users\User $user
     * @param \App\Entities\Users\User $user
     *
     * @return mixed
     */
    public function delete(User $user, ProductType $producttype)
    {
        return $user->hasRole('admin');
    }
}