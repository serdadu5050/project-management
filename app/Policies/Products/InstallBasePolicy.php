<?php

namespace App\Policies\Products;

use App\Entities\Users\User;
use App\Entities\Products\InstallBase;
use Illuminate\Auth\Access\HandlesAuthorization;

class InstallBasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user.
     *
     * @param \App\Entities\Users\User $user
     * @param \App\Entities\Users\User $user
     *
     * @return mixed
     */
    public function view(User $user, InstallBase $installbase)
    {
        return true;
    }

    /**
     * Determine whether the user can create users.
     *
     * @param \App\Entities\Users\User $user
     * @param \App\Entities\Users\User $user
     *
     * @return mixed
     */
    public function create(User $user, InstallBase $installbase)
    {
        return true;
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param \App\Entities\Users\User $user
     * @param \App\Entities\Users\User $user
     *
     * @return mixed
     */
    public function update(User $user, InstallBase $installbase)
    {
        return true;
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param \App\Entities\Users\User $user
     * @param \App\Entities\Users\User $user
     *
     * @return mixed
     */
    public function delete(User $user, InstallBase $installbase)
    {
        return $user->hasRole('admin');
    }
}