<?php

namespace App\Http\Controllers\Products;

use Illuminate\Http\Request;
use App\Entities\Products\Product;
use App\Entities\Partners\Customer;
use App\Entities\Products\InstallBase;
use App\Http\Controllers\Controller;


class InstallBasesController extends Controller
{

    public function index()
    {
        // $installbases = InstallBase::where(function ($query) {
        //     $query->where('customer_id', 'like', '%'.request('q').'%');
        // })
        //     ->paginate(25);

        $custid = request('customer_id');
        if (!empty($custid)){
            $installbases = InstallBase::where('customer_id',$custid)->paginate(25);
        }else{
            $installbases = InstallBase::paginate(25);
        }

        $customers = Customer::orderBy('name')->pluck('name', 'id')->all();

        return view('installbases.index', compact('customers','installbases'));
    }


    public function create()
    {
        $products = Product::orderBy('name')->pluck('name', 'id');
        $customers = Customer::orderBy('name')->pluck('name', 'id');
        return view('installbases.create', compact('products','customers'));
    }


    public function store(Request $request)
    {
        $newInstallBaseData = $this->validate($request, [
            'customer_id'      => 'required',
            'product_id'       => 'required',
            'sn'               => 'required',
        ]);

        InstallBase::create($newInstallBaseData);

        flash(trans('installbase.created'), 'success');

        return redirect()->route('installbases.index');
    }


    public function show(InstallBase $installbase)
    {
        return view('installbases.show', compact('installbase'));
    }


    public function edit(InstallBase $installbase)
    {
        $products = Product::orderBy('name')->pluck('name', 'id');
        $customers = Customer::orderBy('name')->pluck('name', 'id');
        return view('installbases.edit', compact('installbase','customers','products'));
    }


    public function update(Request $request, InstallBase $installbase)
    {
        $installBaseData = $this->validate($request, [
            'customer_id'      => 'required',
            'product_id'       => 'required',
            'sn'               => 'required',
        ]);

        $installbase->update($request->all());

        flash(trans('installbase.updated'), 'success');

        return redirect()->route('installbases.index');
    }


    public function destroy(InstallBase $installbase)
    {
        $this->validate(request(), [
            'installbase_id' => 'required',
        ]);

        $routeParam = request()->only('page', 'q');

        if (request('installbase_id') == $installbase->id && $installbase->delete()) {
            flash(trans('installbase.deleted'), 'warning');

            return redirect()->route('installbases.index', $routeParam);
        }

        flash(trans('installbase.undeleted'), 'danger');

        return back();
    }
}