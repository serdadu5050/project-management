<?php

namespace App\Http\Controllers\Products;

use Illuminate\Http\Request;
use App\Entities\Products\ProductType;
use App\Http\Controllers\Controller;

class ProductTypesController extends Controller
{
    /**
     * Display a listing of the customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $producttypes = ProductType::where(function ($query) {
            $query->where('name', 'like', '%'.request('q').'%');
        })
            ->paginate(25);

        return view('producttypes.index', compact('producttypes'));
    }

    /**
     * Show the create customer form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('producttypes.create');
    }

    /**
     * Store a newly created customer in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newProductTypeData = $this->validate($request, [
            'name'              => 'required',
        ]);

        ProductType::create($newProductTypeData);

        flash(trans('producttype.created'), 'success');

        return redirect()->route('producttypes.index');
    }

    /**
     * Show the specified customer.
     *
     * @param \App\Entities\Partners\Customer $customer
     *
     * @return \Illuminate\Http\Response
     */
    public function show(ProductType $producttype)
    {
        return view('producttypes.show', compact('producttype'));
    }

    /**
     * Show the edit customer form.
     *
     * @param \App\Entities\Partners\Customer $customer
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductType $producttype)
    {
        return view('producttypes.edit', compact('producttype'));
    }

    /**
     * Update the specified customer in storage.
     *
     * @param \Illuminate\Http\Request        $request
     * @param \App\Entities\Partners\Customer $customer
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductType $producttype)
    {
        $productData = $this->validate($request, [
            'name'      => 'required',
        ]);

        $producttype->update($productData);

        flash(trans('producttype.updated'), 'success');

        return redirect()->route('producttypes.index');
    }

    /**
     * Remove the specified customer from storage.
     *
     * @param \App\Entities\Partners\Customer $customer
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductType $producttype)
    {
        // TODO: user cannot delete customer that has been used in other table
        $this->validate(request(), [
            'product_type_id' => 'required',
        ]);

        $routeParam = request()->only('page', 'q');

        if (request('product_type_id') == $producttype->id && $producttype->delete()) {
            flash(trans('producttype.deleted'), 'warning');

            return redirect()->route('producttypes.index', $routeParam);
        }

        flash(trans('producttype.undeleted'), 'danger');

        return back();
    }
}