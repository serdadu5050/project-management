<?php

namespace App\Http\Controllers\Products;

use Illuminate\Http\Request;
use App\Entities\Products\Product;
use App\Entities\Products\ProductType;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * Display a listing of the customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::where(function ($query) {
            $query->where('name', 'like', '%'.request('q').'%');
        })
            ->paginate(25);

        return view('products.index', compact('products'));
    }

    /**
     * Show the create customer form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $producttypes = ProductType::orderBy('name')->pluck('name', 'id');
        return view('products.create', compact('producttypes'));
    }

    /**
     * Store a newly created customer in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newProductData = $this->validate($request, [
            'name'              => 'required',
            'product_type_id'   => 'required',
        ]);

        Product::create($newProductData);

        flash(trans('product.created'), 'success');

        return redirect()->route('products.index');
    }

    /**
     * Show the specified customer.
     *
     * @param \App\Entities\Partners\Customer $customer
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }

    /**
     * Show the edit customer form.
     *
     * @param \App\Entities\Partners\Customer $customer
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $producttypes = ProductType::orderBy('name')->pluck('name', 'id');
        return view('products.edit', compact('product','producttypes'));
    }

    /**
     * Update the specified customer in storage.
     *
     * @param \Illuminate\Http\Request        $request
     * @param \App\Entities\Partners\Customer $customer
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $productData = $this->validate($request, [
            'name'      => 'required',
            'product_type_id'   => 'required',
        ]);

        $product->update($productData);

        flash(trans('product.updated'), 'success');

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified customer from storage.
     *
     * @param \App\Entities\Partners\Customer $customer
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        // TODO: user cannot delete customer that has been used in other table
        $this->validate(request(), [
            'product_id' => 'required',
        ]);

        $routeParam = request()->only('page', 'q');

        if (request('product_id') == $product->id && $product->delete()) {
            flash(trans('product.deleted'), 'warning');

            return redirect()->route('products.index', $routeParam);
        }

        flash(trans('product.undeleted'), 'danger');

        return back();
    }
}