<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLangInProjectModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_model', function($table) {
            $table->string('in_kr',100)->nullable()->after('name');
            $table->string('in_id',100)->nullable()->after('in_kr');
            $table->string('in_en',100)->nullable()->after('in_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
