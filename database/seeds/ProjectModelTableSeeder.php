<?php

use Illuminate\Database\Seeder;
use App\Entities\Projects\ProjectModel;
class ProjectModelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ProjectModel::insert([
            [
            'name'=>'Private'
            ],[
                'name'=>'Government'
            ],[
                'name'=>'Police'
            ],[
                'name'=>'Military'
            ]
            ]);
    }
}
