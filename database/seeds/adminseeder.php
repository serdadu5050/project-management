<?php

use Illuminate\Database\Seeder;
use App\admin;
class adminseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        admin::create([
            'name'=>'admin',
            'email'=>'aan@admin.com',
            'password'=>bcrypt('admin'),
            'remember_token'=>'',
            'api_token'=>'',
            'lang'=>'EN',
            'created_at'=>'',
            'updated_at'=>''

        	
        ]);
    }
}
