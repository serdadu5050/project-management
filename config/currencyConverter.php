<?php

return array (
    
    //API source. 
    //Possible values: 'eurocentralbank' | 'openexchange' | 'yahoo' | 'currencylayer' | 'fixer'
    'api-source' => env('CC_API_SOURCE', 'openexchange'),

    //Your app id from openexchangerates.org
    'openex-app-id' =>  env('CC_OPENEXCHANGE_APP_ID', '2cfc3bb605e74361816efb1d12766e1c'),

    //your API access key for currencylayer.com
    'currencylayer-access-key' => env('CC_CURRENCYLAYER_ACCESS_KEY', ''),

    //your API access key for fixer.io
    'fixer-access-key' => env('CC_FIXERIO_ACCESS_KEY', '4bbc2800b0da10cd44f28bda8e45e1db'),

    //use https? the free version of openexchange and jsonrates does not support https :(
    'use-ssl' => env('CC_USE_SSL', false),

    //When using the free account we can still calculate other currencies based on USD as a base thanks to some basic math.
    //enable this if you want real base values instead of calculated ones. Requires enterprise account from openexchangerates.org
    'openex-use-real-base' => env('CC_USE_REAL_BASE', false),

    //When using the free account we can still calculate other currencies based on EUR as a base thanks to some basic math.
    //enable this if you want real base values instead of calculated ones. Requires payed account on fixer.io
    'fixer-use-real-base' => env('CC_USE_REAL_BASE_FIXER', false),

    //use Laravel cache engine to cache the results.
    'enable-cache' => env('CC_ENABLE_CACHE', true),

    //minutes cache should expire.
    'cache-min' => env('CC_ENABLE_CACHE', 60),

    //use Laravel detailed logging
    'enable-log' => env('CC_ENABLE_LOG', false),

);